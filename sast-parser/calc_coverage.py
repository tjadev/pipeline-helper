import os
import re
import sys

def multiply_values_by_constants(string, constants):
    # Finde alle Zahlen im String
    numbers = re.findall(r'\d+', string)
    # Multipliziere jede Zahl mit der entsprechenden Konstante
    multiplied_values = [int(numbers[i]) * constants[i] for i in range(min(len(numbers), len(constants)))]
    return round(sum(multiplied_values))

def set_factor(severity):
    # Definieren Sie die Multiplikationskonstanten aus Umgebungsvariablen
    env = f'PIPELINE_HELPER_'
    variable = env + severity + f'_FACTOR'
    if variable in os.environ:
        multiplier_constants.append(float(os.environ[variable]))
    else:
        multiplier_constants.append(0.0)

# Überprüfen, ob die korrekte Anzahl an Befehlszeilenargumenten übergeben wurde
if len(sys.argv) != 2:
    print("Verwendung: python script.py <dateiname>")
    sys.exit(1)

# Dateiname aus den Befehlszeilenargumenten extrahieren
file_name = sys.argv[1]

# Überprüfen, ob die Datei existiert
if not os.path.isfile(file_name):
    print(f"Datei '{file_name}' existiert nicht.")
    sys.exit(1)

# Öffnen und lesen der Datei
with open(file_name, 'r') as file:
    file_content = file.read()

# Suchen nach dem Muster "y: [....]"
match = re.search(r'y:\s*\[(.*?)\]', file_content)
if match:
    input_string = match.group(1)
else:
    print("Die Zeile konnte nicht gefunden werden.")
    sys.exit(1)

multiplier_constants = []
set_factor(f"CRITICAL")
set_factor(f"HIGH")
set_factor(f"MEDIUM")
set_factor(f"LOW")
set_factor(f"UNKNOWN")

# Ergebnis
value = multiply_values_by_constants(input_string, multiplier_constants)
print(value)
