decorator==5.1.1
Jinja2==3.1.4
jsonpath-ng==1.7.0
MarkupSafe==3.0.2
ply==3.11
six==1.17.0
packaging==24.2