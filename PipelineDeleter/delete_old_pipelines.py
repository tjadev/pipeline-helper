import requests
import datetime
import os

gitlab_url = "https://gitlab.com"
project_id = os.getenv("CI_PROJECT_ID") #"53625527"
access_token = os.getenv("ACCESS_TOKEN")

if not access_token:
    print("Environment variable ACCESS_TOKEN not found!")
    exit(1)

print(f'Project-ID: {project_id}')
# Set the number of weeks
weeks_to_keep = os.getenv("PIPELINE_HELPER_WEEKS_TO_KEEP")

if not weeks_to_keep:
    weeks_to_keep = 2
else:
    weeks_to_keep = int(weeks_to_keep)  # Convert string to integer

print(f'Weeks to keep: {weeks_to_keep}')

# Calculate the date threshold
if weeks_to_keep > 0:
    threshold_date = datetime.datetime.now() - datetime.timedelta(weeks=weeks_to_keep)
else:
    threshold_date = datetime.datetime.now()

# Get all active branches
branches_response = requests.get(f'{gitlab_url}/api/v4/projects/{project_id}/repository/branches?per_page=1000', headers={'PRIVATE-TOKEN': access_token}, timeout=10)
if branches_response.status_code != 200:
    print(f'{branches_response.json()}')
    raise SystemExit(1)
branches = branches_response.json()

# Get all references from pipelines
allPipelines_response = requests.get(f'{gitlab_url}/api/v4/projects/{project_id}/pipelines?per_page=1000', headers={'PRIVATE-TOKEN': access_token}, timeout=10)
allPipelines = allPipelines_response.json()
print(f'All pipelines: {allPipelines}')

# Collect the latest pipeline ID for each active branch = ref in pipelines
latest_pipelines = {}
# for branch in branches:
for allPipeline in allPipelines:
    print(f'Check pipeline ref: {allPipeline}')
    lastPipeline_response = requests.get(f'{gitlab_url}/api/v4/projects/{project_id}/pipelines?ref={allPipeline["ref"]}&per_page=1', headers={'PRIVATE-TOKEN': access_token}, timeout=10)
    lastPipeline = lastPipeline_response.json()
    if lastPipeline and 'id' in lastPipeline[0]:
        #print(f'Last pipeline for Ref {reference["ref"]} is pipeline {pipeline[0]['id']}')
        latest_pipelines[allPipeline['ref']] = lastPipeline[0]['id']

# Collect pipelines older than the threshold date, excluding the latest pipeline of each branch
pipelines_to_delete = []
for eachPipeline, pipeline_id in latest_pipelines.items():
    pipeline_response = requests.get(f'{gitlab_url}/api/v4/projects/{project_id}/pipelines?ref={eachPipeline}&per_page=1000', headers={'PRIVATE-TOKEN': access_token}, timeout=10)
    pipelines = pipeline_response.json()
    for pipeline in pipelines:
        if pipeline['id'] != pipeline_id:
            created_at = datetime.datetime.strptime(pipeline['created_at'], '%Y-%m-%dT%H:%M:%S.%fZ')
            if created_at < threshold_date:
                pipelines_to_delete.append(pipeline['id'])

# Collect Pipelines with ref to not longer existing branches
for everyPipeline in allPipelines:
    count = 0
    for branch in branches:
        if everyPipeline["ref"] == branch["name"]:
            count = count + 1
    if count == 0:
        created_at = datetime.datetime.strptime(everyPipeline['created_at'], '%Y-%m-%dT%H:%M:%S.%fZ')
        if created_at < threshold_date:
            pipelines_to_delete.append(everyPipeline['id'])

# Delete the collected pipelines
for pipeline_id in pipelines_to_delete:
    response = requests.delete(f'{gitlab_url}/api/v4/projects/{project_id}/pipelines/{pipeline_id}', headers={'PRIVATE-TOKEN': access_token}, timeout=10)
    if response.status_code == 204:
        print(f'Pipeline {pipeline_id} deleted successfully.')
    else:
        print(f'Failed to delete pipeline {pipeline_id}. Status code: {response.status_code}.')
