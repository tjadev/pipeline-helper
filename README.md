# pipeline-helper

This Repo is used to help in gitlab CI/CD pipelines.
There are two helpers included.

The sast-parser, which is used to make the gl-sast-report.json human readable, is a fork from https://github.com/pcfens/sast-parser. Many Thanx to Phil Fenstermacher.

## Features of Pipeline-Helper
1) Gitlab-SAST scanning and using the result for pipeline failure or success
2) Delete old pipelines

## How to integrate in own pipeline (.gitlab-ci.yml)
````
- project: 'tjadev/pipeline-helper'
  file: 'templates/sast-parser-template.yml'
- project: 'tjadev/pipeline-helper'
  file: 'templates/delete-old-pipelines-template.yml'

# Add this stages to your stages
stages:
- scan
- cleanup
````

## How to configure Pipeline-Scanner SAST
The sast-parser generates html-Reports for each scanner which is used by standard gitlab sast-integration.
To make the scanning-result easily visible the scanning result is mapped to the Coverage of the job.
If there are no findings the coverage is 100%. For each finding there is a subtraction which is configurable by pipeline variables per severity of finding.

If the Minimum Value (PIPELINE_HELPER_MIN_COVERAGE with Default 100) is not reached the job will fail. This fail stops the pipeline if the branch is protected or it is a pipeline with a protected target branch.

If you do nothing the job will never fail because the minimum will be 100 but there will be no subtractions because all factors are 0.

````
variables:
    PIPELINE_HELPER_CRITICAL_FACTOR: "25"
    PIPELINE_HELPER_HIGH_FACTOR: "5"
    PIPELINE_HELPER_MEDIUM_FACTOR: "2"
    PIPELINE_HELPER_LOW_FACTOR: "1"
    PIPELINE_HELPER_UNKNOWN_FACTOR: "0.5"
    # Each variable has the default 0. 
    PIPELINE_HELPER_MIN_COVERAGE: "80"
````
## How to configure delete_old_pipelines 
This script could run at last step of your pipeline and deletes all pipelines older than 2 weeks but always keeps the last pipeline of each branch.
1) First you have to **add your gitlab access-token** as CI/CD variable:
   - Settings -> CI/CD -> Variables -> Expand -> Add Variable
   - Type: Variable (default)
   - Environment: All (default)
   - Flags: set **_Mask variable_**
   - Key: ACCESS_TOKEN
   - Value: Token
2) The pipeline variable PIPELINE_HELPER_WEEKS_TO_KEEP can be used to configure how many weeks will be kept. The default is 2.

-------------------------------------------
# Old Readme

## Include sast-parser with template
````
include:
- project: 'tjadev/pipeline-helper'
  file: 'templates/sast-parser-template.yml'

# Add this stages to your stages
stages:
- scan
- parse
````
## Include delete_old_pipelines with template
````
include:
- project: 'tjadev/pipeline-helper'
  file: 'templates/delete_old_pipelines-template.yml'

# Add this stages to your stages
stages:
- cleanup
````
## Include sast-parser directly in the pipeline 
You need to extend your .gitlab-ci.yml
````
include:
- template: Security/SAST.gitlab-ci.yml

stages:
- scan
- parse

sast:
  stage: scan
  after_script:
    - mv gl-sast-report.json gl-sast-report-${CI_JOB_NAME}.json
  artifacts:
    paths:
      - gl-sast-report*.json

sast-parser:
  stage: parse
  image: python:alpine
  script:
    - apk add git
    - git clone https://gitlab.com/tjadev/pipeline-helper.git
    - pip install --no-cache-dir -r pipeline-helper/sast-parser/requirements.txt
    - ls -l pipeline-helper/*
    - ls -l gl-*.json
    - for i in `ls gl-sast-report*.json`; do python pipeline-helper/sast-parser/parse-sast.py ${i} >> gl-sast-report.html; done
  artifacts:
    paths:
      - gl-sast-report.html
````
You can find the SAST-report in the downloadable artifacts.

## Include delete_old_pipelines directly in the pipeline
This script could run at last step of your pipeline and deletes all pipelines older than 2 weeks but always keeps the last pipeline of each branch.
First you have to **add your gitlab access-token** as CI/CD variable:
- Settings -> CI/CD -> Variables -> Expand -> Add Variable 
- Type: Variable (default)
- Environment: All (default)
- Flags: set **_Mask variable_**
- Key: ACCESS_TOKEN
- Value: Token
````
stages:
- cleanup

delete_old_pipelines:
  stage: cleanup
  allow_failure: true
  image: python:alpine
  script:
    - apk add git
    - git clone https://gitlab.com/tjadev/pipeline-helper.git
    - pip install requests
    - python pipeline-helper/delete_old_pipelines.py
````